<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index() {
        // print(asset());
        return view('index');
    }

    public function about() {
        return view('about');
    }
}
