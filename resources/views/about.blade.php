@extends('layouts/base')

@section('content')
<section id="about-page">
    <div class="container pb-5">
        <div class="about" id="about-tedx-ui">
            <p class="title-about">
                ABOUT TEDxUniversitasIndonesia
            </p>
            <p class="content-about">
                TEDxUniversitasIndonesia is a passionate community interested in spreading remarkable ideas to the city’s keenest minds who will take the ideas and use them as a catalyst for change in their respective communities. The organizers are part of what is widely recognized as a community of the most idealistic, innovative and visionary people on the planet. Dubbed the ‘Conference of Cool’ by The Financial Times, the series held in California by the nonprofit TED organization, has since 1984 garnered a devoted following on the Internet in recent years after it made videos of its talks available online. All TEDxUniversitasIndonesia events have been 100% community driven. The speakers are not paid and the organizers are unpaid volunteers. Past TEDxJakarta talks have been oversubscribed by between 50 and 100 per cent. All talks are recorded and available online for free.
            </p>
        </div>
        <div class="about" id="about-tedx">
            <p class="title-about">
                ABOUT TEDx
            </p>
            <p class="content-about">
                In the spirit of ideas worth spreading, TEDx is a program of local, self-organized events that bring people together to share a TED-like experience. At a TEDx event, TEDTalks video and live speakers combine to spark deep discussion and connection in a small group. These local, self-organized events are branded TEDx, where x=independently organized TED event. The TED Conference provides general guidance for the TEDx program, but individual TEDx events are self-organized. Find out more about TEDx at www.ted.com/tedx
            </p>
        </div>
        <div class="about" id="about-ted">
            <p class="title-about">
                ABOUT TED
            </p>
            <p class="content-about">
                TED is a nonprofit devoted to spreading ideas, usually in the form of short, powerful talks (18 minutes or less). TED began in 1984 as a conference where Technology, Entertainment and Design converged, and today covers almost all topics — from science to business to global issues — in more than 100 languages. Meanwhile, independently run TEDx events help share ideas in communities around the world. For more information, visit www.ted.com
            </p>
        </div>
    </div>

</section>
@endsection