<!-- Footer -->
<footer>
	<div class="container pt-3 pb-3 mt-0">
		<div class="row align-items-center">
			<div class="col-sm" style="color: white;">
				This independent TEDx event is operated under license from TED.
			</div>
			<div class="col-sm">
				<div class="float-right" id="socialMedia" style="font-size: 2rem;">
					<a href="https://twitter.com/tedxui" class="pr-3"><i class="fab fa-twitter"></i></a>
					<a href="https://www.instagram.com/tedxui/"><i class="fab fa-instagram"></i></a>	
				</div>
			</div>
		</div>
	</div>
</footer>
