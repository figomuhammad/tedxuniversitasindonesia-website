<div class="container">
	<nav class="navbar navbar-expand-lg navbar-light bg-white">
		<a class="navbar-brand" href="{{url('/')}}">
			<img src="{{asset('img/TEDxUI_WHITE (1)@0,3x.png')}}" width="auto" height="45" alt="TEDxUniversitas Logo">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
	
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="{{url('/')}}">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{url('about/')}}">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="https://www.ted.com/">TED.com</a>
				</li>
			</ul>
		</div>
	</nav>
</div>