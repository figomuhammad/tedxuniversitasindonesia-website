@extends('layouts/base')

@section('content')
	<section id="homepage">
		<section id="blackSection" class="">
			<div class="container text-center pt-5 pb-5">
				<h1 id="imagineFutureText">
					TEDxUniversitasIndonesia: “Imagine the Future of Us”
				</h1>
				<p class="blackSectionText">
					Saturday, 14 March 2019<br>Universitas Indonesia, Depok, Jawa Barat
				</p>
				<a id="grabTicketBtn"class="btn btn-primary" href="https://www.loket.com/event/tedxuniversitasindonesia"
					target="_blank" role="button">Register</a>
			</div>
			<div class="container">
				<div class="row text-center mb-5 justify-content-center">
					<div class="col-sm-2">
						<div id="days" class="time"></div>
					</div>
					<div class="col-sm-2">
						<div id="hours" class="time"></div>
					</div>
					<div class="col-sm-2">
						<div id="minutes" class="time"></div>
					</div>
					<div class="col-sm-2">
						<div id="seconds" class="time"></div>
					</div>
				</div>
			</div>
			<div class="container text-center">
				<p class="blackSectionText">Read further about TEDxUniversitasIndonesia
					<br><i class="material-icons">keyboard_arrow_down</i>
				</p>
					
			</div>
		</section>
		<div class="container mt-5">
			<h3 class="text-center">TEDxUniversitasIndonesia 2020: "Imagine the Future of Us"</h3>	
			<div class="row align-items-center">
				<div class="col-sm">
					<img class="img-fluid" src="{{asset('img/tedx-is.jpg')}}" alt="Photo of TEDx Conference">
				</div>
				<div class="col-sm">
					<p>
						We’ve discussed how we learned, unlearned, and discovered ourselves through our past and future experiences in the pre-event of TEDxUniversitas Indonesia 2020.
						<br>For you guys who missed it, here’s the main show for all of you!
						<br>We invited you to broaden our discussion in the awaited main event of TEDxUniversitasIndonesia: “Imagine the Future of Us”.  We’ll be talking, spreading, and contemplating ideas concerning the future of social equality, art & technology, healthcare, personal growth— all the possible future of us with multidisciplinary speakers and performers and their respective unique stories.
						<br>So get yourself a ticket and prepare your answers to this year’s TEDxUniversitasIndonesia primary question: “How do you imagine the ideal future of us?”
					</p>
				</div>
			</div>
			<hr class="mb-5" style="border-color: #e2372e;">
			<div class="container text-center">
				<h3>Our Speakers</h3>
			</div>
			<div class="row">
				<div class="col-sm">
					<div class="container">
						<img class="img-fluid" src="{{asset('img/speakers/farisrahman1.jpg')}}" alt="Photo of Faris Rahman">
					</div>
					<div class="container text-center mt-3 mb-3">
						<h5>Faris Rahman</h5>
						<h6>“Co-founder and CTO of Nodeflux”</h6>
					</div>
					<div class="container">
						<p>
						Faris is an exceptional engineer passionate for advanced and innovative technology, along with the progressive entrepreneurial mindset and thought-leadership skill. 
						With a Bachelor's Degree in Industrial Engineering he earned from Bandung Institute of Technology (ITB) as his essential capital, Faris continues to gain knowledge in the field of computation.
						His self-taught exploration goes deep in the expertise of distributed computation, parallel computing, high-performance computing, and artificial intelligence. Before Nodeflux, he held a series of working- experience in the area of engineering.
						</p>
					</div>
				</div>
				
				<div class="col-sm">
					<div class="container">
						<img class="img-fluid" src="{{asset('img/speakers/widya1.jpg')}}" alt="Photo of Widya Sawitar">
					</div>
					<div class="container text-center mt-3 mb-3">
						<h5>Widya Sawitar</h5>
						<h6>“Pecinta Langit”</h6>
					</div>
					<div class="container">
						<p>
						Widya is an astronomy alumni from ITB and a partner of HAAJ (Jakarta Amateur Astronomy Association). He actively introduce astronomy to the public at the Jakarta Planetarium and Observatory.
						Widya also fostered and actively conducted research to trace the links of Indonesian astronomy and culture.
						For his work in carrying out the development of STEAM (Science, Tech, Engineering, Art, and Math) at national and international level, Widya receive Ganesa Widya Jasa Adiutama award from ITB in 2018. In addition, Widya was also involved as a member of the Editor's team in the Stars of Asia Project, a project to collect folklore related to astronomy in the Asia Pacific.
						</p>
					</div>
				</div>

				<div class="col-sm">
				<div class="container">
						<img class="img-fluid" src="{{asset('img/speakers/manik1.jpg')}}" alt="Photo of Manik Marganamahendra">
					</div>
					<div class="container text-center mt-3 mb-3">
						<h5>Manik Marganamahendra</h5>
						<h6>“Social Activism Enthusiast”</h6>
					</div>
					<div class="container">
						<p>
						Since high school junior year, Manik feels very happy and love to be involved in organization activity. For him, developing himself with his friends is something very exciting. Aside of giving comfort in his organization, Manik really enjoy the process of learning about his friends idea from the most homogenous group the the most heterogeneous.
						His best reason to keep being in an organization and knowing lots of people is to be the best a human can be that learn and be impactful to others. That journey is what brought him to lead various student body from faculty level to university until initiating various programs. One of them is initiating Southeast Asia citizen’s health movement collaborating with student and NGO in Thailand and Singapore. 
						</p>
					</div>
				</div>
				
			<div class="row mt-5 justify-content-center">
				<div class="col-sm-4">
					<div class="container">
							<img class="img-fluid" src="{{asset('img/speakers/drterry1.jpg')}}" alt="Photo of Prof. Dr. Terry Mart">
					</div>
					<div class="container text-center mt-3 mb-3">
						<h5>Prof. Dr. Terry Mart</h5>
						<h6>“Professor of Physics”</h6>
					</div>
					<div class="container">
						<p>
						Dr. Terry Mart is a full-time professor at University of Indonesia. For decades, he has conducted research on the theoretical nuclear and particle physics field. His works has been acknowledged and cited by physicists working on related subjects worldwide.
						Having received many awards, including the Habibie Award for pioneering theoretical research in Indonesia, and most recently, the LIPI Sarwono Award for his dedication on the research field, Terry thinks that there are more to be done on the field, and has no intention to stop doing research.
						As a researcher as well as an educator, Terry is dedicated to the development of research and science, and hopes to promote the theoretical physics field to the scientific community. 
						</p>
					</div>
				</div>
				
				<div class="col-sm-4">
				<div class="container">
						<img class="img-fluid" src="{{asset('img/speakers/bahreisy1.jpg')}}" alt="Photo of Bahreisy Imadudien">
					</div>
					<div class="container text-center mt-3 mb-3">
						<h5>Bahreisy Imadudien</h5>
						<h6>“Meaning Cultivator”</h6>
					</div>
					<div class="container">
						<p>
						Bahreisy is a psychology student with a specific interest in cultivating personal meaning.
						Pursuing this end, Bahreisy utilizes stories and fiction emphasizing personal narratives of individuals advocating the voluntary acceptance of personal responsibility.
						This ideal is manifested in various endeavors Bahreisy undertakes. Bahreisy was the champion of the 2020 Asian English Olympics in the field of storytelling. He also produces video essays analyzing the psychological and philosophical significances of fiction and fictional characters. Lastly, he writes and performs songs expressing various existential struggles framed within a personal narrative.
						The central notion of personal responsibility characterizes Bahreisy’s upcoming talk. This concept will be elaborated within the dynamics of modern dating, romantic rejections, and the quest for love.
						</p>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="container">
							<img class="img-fluid" src="{{asset('img/speakers/wildan1.jpg')}}" alt="Photo of Muh. Wildan Teddy Bintang P. Has">
					</div>
					<div class="container text-center mt-3 mb-3">
						<h5>Muh. Wildan Teddy Bintang P. Has</h5>
						<h6>“Coordinator of HopeHelps UI”</h6>
					</div>
					<div class="container">
						<p>
						Having grown up witnessing violence against women and children, Teddy has become a passionate law student, researcher, and advocate for women’s & LGBT rights. He has been working with his team at HopeHelps UI (@hopehelps.ui) to advocate for sexual violence victims on campus.
						Since 2014, Teddy has been involved in the issues of human rights, such as sexual violence; discrimination against the LGBT community; domestic violence; child protection; migrant workers; adat community; and women’s access to natural resources.
						Teddy’s works have been published by Indonesian Judicial Reform Forum, Global Landscapes Forum, and Australian Journal of Human Rights. He contributed to the UNDP Jakarta’s work on gender and business integrity. He was also chosen by U.S. Department of State to be a part of Summer 2019 Study of the United States Institutes (SUSI) for Student Leaders.
						In his leisure time, Teddy enjoys watching drag race, listening to Motown music, and sleeping.
						</p>
					</div>
				</div>
			</div>
		</div>
		<!--<div class="container">
			<hr class="mb-5" style="border-color: #e2372e;">
			<div class="container text-center">
				<h3>Our Sponsors</h3>
			</div>
		</div> -->
		
		<div class="container mt-5 text-center">
			<a id="grabTicketBtn"class="btn btn-primary" href="https://www.loket.com/event/tedxuniversitasindonesia"
				target="_blank" role="button">Register</a>
		</div>

		<script>
			var countDownDate = new Date("Mar 14, 2020 09:00:00").getTime()
			console.log(countDownDate);
			var x = setInterval(function() {
				var now = new Date().getTime();

				var distance = countDownDate - now;

				var days  = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				if (days == 1) {
					document.getElementById("days").innerHTML = days + "<br>Day";
				}
				else {
					document.getElementById("days").innerHTML = days + "<br>Days";
				}

				if (hours == 1) {
					document.getElementById("hours").innerHTML = hours + "<br>Hour";
				}
				else {
					document.getElementById("hours").innerHTML = hours + "<br>Hours";
				}

				if (minutes == 1) {
					document.getElementById("minutes").innerHTML = minutes + "<br>Minute";
				}
				else {
					document.getElementById("minutes").innerHTML = minutes + "<br>Minutes";
				}
				if (seconds == 1) {
					document.getElementById("seconds").innerHTML = seconds + "<br><span>Second</span>";
				}
				else {
					document.getElementById("seconds").innerHTML = seconds + "<br><span>Seconds</span>";
				}

				//TODO
				/* 
				if (distance < 0) {
					clearInterval(x)
					document.getElementById("jam").innerHTML = "EXPIRED"
				} */
			})
		</script>
	</section>
@endsection